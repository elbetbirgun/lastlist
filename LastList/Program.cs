﻿using ClosedXML.Excel;
using LinqToExcel;
using LinqToExcel.Query;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LastList
{
    class Program
    {
        static void Main()
        {

            string path = Path.GetFullPath(@"C:\Users\399771\Desktop\lastlist.xls");
            string path2 = Path.GetFullPath(@"C:\Users\399771\Desktop\tümcihazlistesi.xls");

   
            var excelQueryFactory = new ExcelQueryFactory(path)
            {
                ReadOnly = true,
                TrimSpaces = TrimSpacesType.Both,
                UsePersistentConnection = true

            };

            var excelQueryFactory2 = new ExcelQueryFactory(path2)
            {
                ReadOnly = true,
                TrimSpaces = TrimSpacesType.Both,
                UsePersistentConnection = true
            };

            int i = 2;

            var Excel = excelQueryFactory.Worksheet<Trial>("Sheet4").ToList().OrderBy(x => x.GSM);
            var Excel2 = excelQueryFactory2.Worksheet<Trial2>("TümCihazListesi").ToList();


            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Hatlar");

                worksheet.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                worksheet.FirstRow().Style.Font.SetBold(true);


                var A1 = worksheet.Cell("A1");
                A1.Value = "PROJE SERİ NO";


                var B1 = worksheet.Cell("B1");
                B1.Value = "GSM";


                var C1 = worksheet.Cell("C1");
                C1.Value = "OCAK";


                var D1 = worksheet.Cell("D1");
                D1.Value = "ŞUBAT";


                var E1 = worksheet.Cell("E1");
                E1.Value = "MART";


                var F1 = worksheet.Cell("F1");
                F1.Value = "NİSAN";


                var G1 = worksheet.Cell("G1");
                G1.Value = "TOPLAM DEĞER";


                var H1 = worksheet.Cell("H1");
                H1.Value = "BİRİM";

                var I1 = worksheet.Cell("I1");
                I1.Value = "PROJE";


                var J1 = worksheet.Cell("J1");
                J1.Value = "Cihaz seri no";


                var K1 = worksheet.Cell("K1");
                K1.Value = "Sim imei no";


                var L1 = worksheet.Cell("L1");
                L1.Value = "Cihaz marka";


                var M1 = worksheet.Cell("M1");
                M1.Value = "Cihaz model";


                Excel.Select(x => new
                {

                    x.GSM,
                    ToplamDeger = x.Mar.ToInt32OrDefault() + x.Nis.ToInt32OrDefault() + x.Sub.ToInt32OrDefault() + x.Oca.ToInt32OrDefault(),
                    x.Oca,
                    x.Mar,
                    x.Sub,
                    x.Nis

                }).GroupJoin(Excel2, a => a.GSM, b => b.simhatno, (c, d) => new
                {
                    c.Oca,
                    c.Mar,
                    c.Sub,
                    c.Nis,
                    c.GSM,
                    c.ToplamDeger,
                    İkinciListe = d

                }).SelectMany(x => x.İkinciListe.DefaultIfEmpty(), (a, b) => new
                {
                    a.Oca,
                    a.Mar,
                    a.Sub,
                    a.Nis,
                    a.GSM,
                    a.ToplamDeger,
                    İkinciListe = b

                })
             .OrderBy(x => x.ToplamDeger)
             .ToList().ForEach(x =>
                   {
                      
                       var B = worksheet.Cell("B" + i);
                       B.SetValue(x.GSM);

                       var C = worksheet.Cell("C" + i);
                       C.SetValue(x.Oca == "-" ? "0 KB" : x.Oca + " KB");

                       var D = worksheet.Cell("D" + i);
                       D.SetValue(x.Sub == "-" ? "0 KB" : x.Sub + " KB");

                       var E = worksheet.Cell("E" + i);
                       E.SetValue(x.Mar == "-" ? "0 KB" : x.Mar + " KB");


                       var F = worksheet.Cell("F" + i);
                       F.SetValue(x.Nis == "-" ? "0 KB" : x.Nis + " KB");


                       var G = worksheet.Cell("G" + i);
                       G.SetValue(((int)x.ToplamDeger / 1024 + "MB"));

                       if (x.İkinciListe != null)
                       {

                           var A = worksheet.Cell("A" + i);
                           A.SetValue(x.İkinciListe.projeserino);

                           var H = worksheet.Cell("H" + i);
                           H.SetValue(x.İkinciListe.birim);

                           var I = worksheet.Cell("I" + i);
                           I.SetValue(x.İkinciListe.proje);


                           var J = worksheet.Cell("J" + i);
                           J.SetValue(x.İkinciListe.cihazserino);


                           var K = worksheet.Cell("K" + i);
                           K.SetValue(x.İkinciListe.simimeino);

                           var L = worksheet.Cell("L" + i);
                           L.SetValue(x.İkinciListe.cihazmarka);


                           var M = worksheet.Cell("M" + i);
                           M.SetValue(x.İkinciListe.cihazmodel);

                       }


                       i++;

                   });


                workbook.SaveAs("Hatlar.xlsx");
            }

        }

    }


    public static class IntExtension
    {
        public static double ToInt32OrDefault(this string value, int defaultValue = 0)
        {
            return double.TryParse(value, out double result) ? result : defaultValue;
        }
    }
}
