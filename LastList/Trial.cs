﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LastList
{
    public class Trial
    {
        public long GSM { get; set; }

        public string Oca { get; set; }

        public string Sub { get; set; }

        public string Mar { get; set; }

        public string Nis { get; set; }

    }
}
